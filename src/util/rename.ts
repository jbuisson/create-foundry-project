import path from 'path';
import fs from 'fs-extra';
import chalk from 'chalk';

function makePath(base = __dirname, file: string) {
	return path.join(base, file);
}

async function handleFile(
	file: string,
	filePath: string,
	src: string,
	dest: string,
	debug = false
) {
	const splitFilePath = filePath.split('.');
	const filePathNoExt = splitFilePath[0];
	const srcExt = splitFilePath[1];

	if (srcExt === src) {
		if (debug) console.log(`${chalk.blue('Renamed: ')} ${chalk.green(filePath)} --> ${chalk.green(`${filePathNoExt}.${dest}`)}`);
		await fs.rename(filePath, `${filePathNoExt}.${dest}`);
	}
}

export default class Rename {
	constructor(
		private src: string,
		private dest: string,
		private startDir: string,
		private debug = false
	) {}

	async traverse(dir: string = this.startDir) {
		const files = await fs.readdir(dir);
	
		for (const file of files) {
			const filePath = makePath(dir, file);
			const stats = await fs.stat(filePath);
	
			if (stats.isDirectory()) {
				await this.traverse(filePath);
			} else {
				await handleFile(file, filePath, this.src, this.dest, this.debug);
			}
		}
		return Promise.resolve();
	}
}
