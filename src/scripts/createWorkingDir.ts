import fs from 'fs-extra';
import chalk from 'chalk';
import ora from 'ora';

export default async (dir: string, force = false) => {
	const spinner = ora('Initializing working directory').start();

	try {
		if (fs.existsSync(dir)) {
			const content = await fs.readdir(dir);
			if (content.length !== 0) {
				console.log();
				
				if (force) {
					spinner.warn(chalk.yellow(`The specified path ${chalk.green(dir)} is not empty`));
					console.log(chalk.yellow('  "--force" was set, existing files will be lost'));
					console.log(chalk.yellow('  Overwriting...'));
					await fs.emptyDir(dir);
				} else {
					spinner.fail(chalk.red(`The specified path ${chalk.green(dir)} is not empty`));
					console.error(chalk.red(`  Use ${chalk.blueBright('-f, --force')} to overwrite an existing directory`));
					console.error(chalk.red('  Aborting...'));
					throw new Error('The specified path is not empty');
				}
			}
		}

		await fs.ensureDir(dir);
	} catch (err) {
		spinner.fail(chalk.red('Failed to create working directory'));
		throw err;
	}

	spinner.succeed(chalk.green('Created working directory'));
};
