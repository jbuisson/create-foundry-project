import path from 'path';
import chalk from 'chalk';

import createPackage from './createPackage';
import createWorkingDir from './createWorkingDir';
import createStructure from './createStructure';
import installDeps from '../util/installDeps';
import createConfig from './createConfig';
import initGit from './initGit';

export default async (
	root: string,
	isSystem: boolean,
	useTypeScript: boolean,
	cssPrep: 'less' | 'sass' | undefined,
	useLint: boolean,
	overwrite: boolean,
	deps: boolean,
	git: boolean
) => {
	const dir = path.resolve(root);
	const name = path.basename(dir);

	const options: ProjectOptions = {
		dir,
		name,
		isSystem,
		useTypeScript,
		cssPrep,
		useLint,
		deps,
		git,
	};

	console.log(
		chalk.bold(`Creating a new Foundry project in ${chalk.green(dir)}`)
	);
	console.log();

	try {
		await createWorkingDir(dir, overwrite);

		await createPackage(options);
		await createStructure(options);
		await initGit(options);
		await createConfig(options);
		await installDeps(options);
	} catch (err) {
		console.error(chalk.red(`Project creation failed with error: ${err}`));
		process.exit(1);
	}
};
